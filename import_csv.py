import csv

from datetime import datetime
from app.database.weather_repository import WeatherRepository

weather_repository = WeatherRepository()

def readFile(nameFile):
    with open(nameFile, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')

        headers = next(spamreader, None)

        for row in spamreader:
            dictRow = {}
            for i in range(len(headers)):
                dictRow[headers[i]] = row[i]

            record = {
                "dt_iso": datetime.fromtimestamp(int(dictRow['dt'])),
                "metaData": {
                    "origin": "openweather",
                    "id": 1,
                    "id_station": 4, # La Palma
                    "city_name": "Navolato", # La Palma
                    "timezone": str(dictRow['timezone']),
                    "lat": float(dictRow['lon']),
                    "lon": float(dictRow['lat']),
                },
                "temp": float(dictRow['temp']),
                "feels_like": float(dictRow['feels_like']),
                "temp_min": float(dictRow['temp_min']),
                "temp_max": float(dictRow['temp_max']),
                "pressure": float(dictRow['pressure']),
                "humidity": float(dictRow['humidity']),
                "wind_speed": float(dictRow['wind_speed']),
                "wind_deg": float(dictRow['wind_deg']),
            }
            weather_repository.save(record)

# Execute function
if __name__ == "__main__":
    readFile('la_palma.csv')