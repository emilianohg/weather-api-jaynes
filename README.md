# Weather API
## Tabla de contenido
1. [Informacion General](#informacion-general)
2. [Tecnologias](#tecnologias)
3. [Instalación](#instalación)
4. [Colaboradores](#colaboradores)

## Información general
El servicio Weather API de growmind registra el clima para su posterior uso en procesos de Inteligencia Artificial.


## Tecnologias
Lista de tecnologias usadas en el proyecto:
* [Python](https://example.com)
* [MongoDb](https://example.com)
* [Docker](https://example.com)
* [FastApi](https://example.com)

## Instalación 

Crear archivo (.env) copiando como ejemplo .(env.example):
```
MONGO_HOST= "Dirección del servidor de base de datos MongoDB."
MONGO_USERNAME= "Asignar usuario"
MONGO_PASSWORD= "Asignar contraseña"
MONGO_DATABASE= "Nombre de la base de datos (weather)"
 
OPENWEATHER_API_KEY= "Agregar api key para obtener información meteorológica de la API de OpenWeather"

#Define el tiempo en segundos para llamar a la API de OpenWeather
OPENWEATHER_API_TIME=60
```
### Crear base de datos weather en MongoDb  
#### Registrar los archivos.json que se encuentran en la carpeta (collections)
```
Crear colleción stations y registrar stations.json
```


## Ejecución en ambiente de desarrollo
```bash
docker compose -f docker-compose.dev.yml up --build
```

## Colaboradores
* [Emiliano HG](https://gitlab.com/emilianohg) 
* [Jesus Lizarraga](https://gitlab.com/JesusLizarraga)



