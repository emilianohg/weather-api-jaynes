from fastapi import FastAPI, HTTPException

from dotenv import dotenv_values
from fastapi_utils.tasks import repeat_every
from starlette.middleware.cors import CORSMiddleware
from typing import Optional
import pandas as pd


from app.config.logging import getLogger

from datetime import datetime
from app.services.calc_attributes import calc_resample, calc_attributes
from app.services.validate_datetime import is_valid_datetime_format
from app.services.openweather import get_openweather_data
from app.database.weather_repository import WeatherRepository
from app.database.stations_repository import StationsRepository
from app.database.cultivos_repository import CultivosRepository

config = dotenv_values(".env")

logger = getLogger(__name__)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    max_age=3600,
)

stations_repository = StationsRepository()
cultivos_repository = CultivosRepository()


@app.on_event("startup")
@repeat_every(
    seconds=1800,
    wait_first=True,
    raise_exceptions=False,
    logger=logger
)
def read_root():

    weather_repository = WeatherRepository()

    cultivos = cultivos_repository.findAll()
    
    logger.info(f"Cultivos: {len(cultivos)}")

    logger.info(f"Download weather {datetime.now()}")
    
    stations = stations_repository.findAll()
    logger.info(f"Total of stations: {len(stations)}")

    for station in stations:
        logger.info(
            f"Getting data from openweather for station: {station.name}"
        )
        record = get_openweather_data(station.lat, station.lng, station.id)
        weather_repository.save(record)

    logger.info(
        f"Download weather finished {datetime.now()}"
    )


@app.get("/weather/{idStation}")
def get_weather(
    idStation: int,
    idCultivo: int,
    start: Optional[str] = None,
    stop: Optional[str] = None,
    aggUnit: Optional[str] = 'hour'
):    
    logger.info(
        f"Getting weather data for station: {idStation} idCultivo: {idCultivo} start: {start} stop: {stop} aggUnit: {aggUnit}"
    )

    if idCultivo is None:
        logger.error(f"El idCultivo es requerido.")
        raise HTTPException(
            status_code=400,
            detail="El idCultivo es requerido."
        )

    cultivo = cultivos_repository.findById(idCultivo)

    if cultivo is None:
        logger.error(f"No se encontró el cultivo con id: {idCultivo}.")
        raise HTTPException(
            status_code=404,
            detail=f"No se encontró el cultivo con id: {idCultivo}."
        )

    if start is not None and not is_valid_datetime_format(start):
        logger.error(
            f"El formato de 'start' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )
        raise HTTPException(
            status_code=400,
            detail="El formato de 'start' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )

    if stop is not None and not is_valid_datetime_format(stop):
        logger.error(
            f"El formato de 'stop' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )
        raise HTTPException(
            status_code=400,
            detail="El formato de 'stop' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )

    station = stations_repository.findById(idStation)

    if station is None:
        logger.error(f"No se encontró la estación con id: {idStation}.")
        raise HTTPException(
            status_code=404,
            detail=f"No se encontró la estación con id: {idStation}."
        )
    
    weather_repository = WeatherRepository()
    
    weather = weather_repository.find(station, start, stop, aggUnit)

    if weather is None or len(weather) == 0:
        logger.error(
            f"No se encontraron datos para la estación y fechas especificadas."
        )
        raise HTTPException(
            status_code=404,
            detail="No se encontraron datos para la estación y fechas especificadas."
        )

    df = pd.DataFrame(weather)

    data = calc_attributes(df, cultivo)

    resample = calc_resample(data, cultivo)

    return {
        "station": station,
        "data": data.to_dict(orient='records'),
        "resampledata": resample.to_dict(orient='records')
    }


@app.get("/weather/daydegrees/{idStation}")
def weather(
    idStation: int,
    idCultivo: int,
    start: Optional[str] = None,
    stop: Optional[str] = None,
    aggUnit: Optional[str] = 'day'
):

    if idCultivo is None:
        logger.error(f"El idCultivo es requerido.")
        raise HTTPException(
            status_code=400,
            detail="El idCultivo es requerido."
        )

    cultivo = cultivos_repository.findById(idCultivo)

    if cultivo is None:
        logger.error(f"No se encontró el cultivo con id: {idCultivo}.")
        raise HTTPException(
            status_code=404,
            detail="No se encontró el cultivo especificado."
        )

    if start is not None and not is_valid_datetime_format(start):
        logger.error(
            f"El formato de 'start' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )
        raise HTTPException(
            status_code=400,
            detail="El formato de 'start' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )

    if stop is not None and not is_valid_datetime_format(stop):
        logger.error(
            f"El formato de 'stop' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )
        raise HTTPException(
            status_code=400,
            detail="El formato de 'stop' no es válido. Utilice el formato 'YYYY-MM-DD'."
        )
    
    station = stations_repository.findById(idStation)

    if station is None:
        logger.error(f"No se encontró la estación con id: {idStation}.")
        raise HTTPException(
            status_code=404,
            detail=f"No se encontró la estación con id: {idStation}."
        )
    
    weather_repository = WeatherRepository()
    
    weather = weather_repository.find(station, start, stop, aggUnit)

    if weather is None or len(weather) == 0:
        logger.error(
            f"No se encontraron datos para la estación y fechas especificadas."
        )
        raise HTTPException(
            status_code=404,
            detail="No se encontraron datos para la estación y fechas especificadas."
        )

    df = pd.DataFrame(weather).reset_index().set_index('time')

    df["hd"] = ((df['temp_min'] + df['temp_max']) / 2) - \
        cultivo.ltresh_degree_days

    df['hd_acum'] = df['hd'].cumsum()

    df.sort_values(by="time", ascending=False, inplace=True)

    df = df.fillna('')

    last_hour_degrees_acum = df['hd_acum'].iloc[0]

    return {
        'hora_calor_ultimo_dia': last_hour_degrees_acum
    }
