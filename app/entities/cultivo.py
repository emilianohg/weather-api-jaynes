class Cultivo:
    def __init__(self, station_dict):
        self.id = station_dict.get("id")
        self.name = station_dict.get("name")
        self.ltresh_degree_days = station_dict.get("ltresh_degree_days")

    def __str__(self):
        return f"Cultivo ID: {self.id}, Name: {self.name}, " \
               f"Ltresh_degree_days: {self.ltresh_degree_days}"