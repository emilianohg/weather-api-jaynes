class Station:
    def __init__(self, station_dict):
        self.id = station_dict.get("id")
        self.name = station_dict.get("name")
        self.description = station_dict.get("description")
        self.lat = station_dict.get("lat")
        self.lng = station_dict.get("lng")
        self.timezone = station_dict.get("timezone")

    def __str__(self):
        return f"Station ID: {self.id}, Name: {self.name}, " \
               f"Description: {self.description}, " \
               f"Latitude: {self.lat}, Longitude: {self.lng}" \
               f"Timezone: {self.timezone}"