import pandas as pd
import numpy as np

from app.entities.cultivo import Cultivo

def svp(temp):
    """Saturated Vapor Pressure "SVP"
    in: temp in celcius
    output: pressure in kilopascales
    """
    svp = 0.6108*np.exp((17.27*temp)/(temp+237.3))
    return svp


def avp(svp, rh):
    """Active Vapor Pressure "AVP"
    in: SVP, rh (relative humidity)
    out: avp"""
    avp = svp*(rh/100)
    return avp


def vpd(temp, rh):
    """Vapor Pressure Deficit "VPD"
    in: temp, rh(relative humidity)
    out: vpd """
    svp = 0.6108*np.exp((17.27*temp)/(temp+237.3))
    avp = svp*(rh/100)

    return svp - avp

def calc_resample(df, cultivo: Cultivo):
    resample = df.copy()
    resample['time'] = pd.to_datetime(resample['time'])
    resample.set_index('time', inplace=True)
    
    resample = resample[['temp']].resample('1D').agg(
        [
            ('suma_temp', 'sum'),
            ('temp', 'mean'),
            ('temp_min', 'min'),
            ('temp_max', 'max')
        ]
    )['temp']

    resample["hd"] = ((resample['temp_min'] + resample['temp_max']) / 2) - cultivo.ltresh_degree_days

    resample['hd_acum'] = resample['hd'].cumsum()

    resample = resample.fillna('')
    resample = resample.reset_index()

    return resample



def calc_attributes(df, idCultivo):

    df['svp']  = df['temp'].apply(svp)

    df['avp']  = avp(df['svp'], df['humidity'])

    df['vpd']  = vpd(round(df['temp'], 2), round(df['humidity'], 2))

    df = df.fillna('')
    
    return df