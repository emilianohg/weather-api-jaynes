import re

def is_valid_datetime_format(datetime_str: str) -> bool:
    '''Valida que la cadena de texto cumpla con el formato de fecha y hora ISO 8601'''
    pattern = r'^\d{4}-\d{2}-\d{2}$'
    return bool(re.match(pattern, datetime_str))