from datetime import datetime, timedelta

import requests
from dotenv import dotenv_values

from app.config.logging import getLogger

config = dotenv_values(".env")

logger = getLogger(__name__)


def transform(item, id_station) -> dict:

    return {
        "dt_iso": datetime.fromtimestamp(item['dt']),
        "metaData": {
            "origin": "openweather",
            "id": item["id"],
            "id_station": id_station,
            "city_name": item["name"],
            "timezone": str(item['timezone']),
            "lat": float(item['coord'].get('lat')),
            "lon": float(item['coord'].get('lon')),
        },
        "temp": float(item["main"].get('temp')),
        "feels_like": float(item['main'].get('feels_like')),
        "temp_min": float(item['main'].get('temp_min')),
        "temp_max": float(item['main'].get('temp_max')),
        "pressure": float(item['main'].get('pressure')),
        "humidity": float(item['main'].get('humidity')),
        "wind_speed": float(item['wind'].get('speed')),
        "wind_deg": float(item['wind'].get('deg')),
    }


def get_openweather_data(lat: str, lon: str, id_station: int) -> dict:
    openweather_url = "https://api.openweathermap.org/data/2.5/weather"
    appid = config['OPENWEATHER_API_KEY']

    url = f"{openweather_url}?lat={lat}&lon={lon}&appid={appid}&units=metric"

    logger.info(f"Getting data from openweather: {url}")

    data = requests.get(url, verify=False).json()

    logger.info(f"Data from openweather: {data}")

    return transform(data, id_station)
