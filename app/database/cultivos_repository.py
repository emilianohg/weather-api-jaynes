from pymongo.collection import Collection
from pymongo.errors import PyMongoError
from app.entities.cultivo import Cultivo

from .connection import MongoDBConnection

from app.config.logging import getLogger

logger = getLogger(__name__)


class CultivosRepository:
    def __init__(self):
        self.mongo_connection = MongoDBConnection()
        self.stations_collection = self.mongo_connection.get_client().weather.cultivos

    def save(self, data):
        try:
            self.stations_collection.insert_one(data)
            logger.info("Cultivos data inserted successfully.")
        except PyMongoError as e:
            logger.error("Error inserting cultivo data:", e)

    def findAll(self) -> list:
        try:
            records = self.stations_collection.find()
            return [Cultivo(record) for record in records]
        except PyMongoError as e:
            logger.error("Error finding cultivo data:", e)

    def findById(self, id) -> Cultivo:
        try:
            record = self.stations_collection.find_one({
                'id': id,
            })

            if not record:
                return None
            
            return Cultivo(record)

        except PyMongoError as e:
            logger.error("Error finding cultivo data:", e)

