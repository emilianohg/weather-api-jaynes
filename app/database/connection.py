from pymongo import MongoClient
from dotenv import dotenv_values

config = dotenv_values(".env")

class MongoDBConnection:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._instance.init_connection()
        return cls._instance

    def init_connection(self):
        self.config = dotenv_values(".env")
        self.client = MongoClient(
            self.config['MONGO_HOST'],
            username=self.config['MONGO_USERNAME'],
            password=self.config['MONGO_PASSWORD'],
        )

    def get_client(self):
        return self.client

    def get_config(self):
        return self.config