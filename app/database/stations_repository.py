from pymongo.collection import Collection
from pymongo.errors import PyMongoError
from app.entities.stations import Station

from .connection import MongoDBConnection

from app.config.logging import getLogger

logger = getLogger(__name__)

class StationsRepository:
    def __init__(self):
        self.mongo_connection = MongoDBConnection()
        self.stations_collection = self.mongo_connection.get_client().weather.stations

    def save(self, data):
        try:
            logger.info("Stations data inserted successfully.")
        except PyMongoError as e:
            logger.error("Error inserting station data:", e)

    def findAll(self) -> list:
        try:
            records = self.stations_collection.find()
            return [Station(record) for record in records]
        except PyMongoError as e:
            logger.error("Error finding station data:", e)

    def findById(self, id) -> Station:
        try:
            record = self.stations_collection.find_one({
                'id': id,
            })

            if not record:
                return None
            
            return Station(record)

        except PyMongoError as e:
            logger.error("Error finding station data:", e)

