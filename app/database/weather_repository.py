from datetime import datetime, timedelta, timezone

from pymongo.errors import PyMongoError

from .connection import MongoDBConnection
from app.entities.stations import Station

from app.config.logging import getLogger

logger = getLogger(__name__)


def get_pipe(station: Station, start: str, stop: str, unit):

    match = {
        'metaData.id_station': {
            '$eq': station.id
        },
    }

    if (start is not None or stop is not None):

        match['dt_iso'] = {}

        if (start is not None): 
            dt_start = datetime.fromisoformat(start) - timedelta(seconds=station.timezone)
            match['dt_iso']['$gte'] = dt_start

        if (stop is not None):
            dt_stop = datetime.fromisoformat(stop) - timedelta(seconds=station.timezone)
            match['dt_iso']['$lt'] = dt_stop

    return [
        {
            "$match": match
        },
        {
            '$group': {
                '_id': {
                    'metaData': '$metaData',
                    'dt_iso': {
                        '$dateTrunc': {
                            'date': '$dt_iso',
                            'unit': unit,
                            'startOfWeek': 'monday',
                            'binSize': 1,
                        },
                    },
                },
                'temp': {'$avg': {'$convert': {'input': '$temp', 'to': 'double'}}},
                'temp_max': {'$avg': {'$convert': {'input': '$temp_max', 'to': 'double'}}},
                'temp_min': {'$avg': {'$convert': {'input': '$temp_min', 'to': 'double'}}},
                'pressure': {'$avg': {'$convert': {'input': '$pressure', 'to': 'double'}}},
                'humidity': {'$avg': {'$convert': {'input': '$humidity', 'to': 'double'}}},
                'wind_speed': {'$avg': {'$convert': {'input': '$wind_speed', 'to': 'double'}}},
                'feels_like': {'$avg': {'$convert': {'input': '$feels_like', 'to': 'double'}}},
                'wind_deg': {'$avg': {'$convert': {'input': '$wind_deg', 'to': 'double'}}},
            }
        },
        {
            '$project': {
                'dt_iso': '$_id.dt_iso',
                'temp': 1,
                'temp_max': 1,
                'temp_min': 1,
                'pressure': 1,
                'humidity': 1,
                'wind_speed': 1,
                'feels_like': 1,
                'wind_deg': 1,
                'metaData': 1,
                'timezone': '$_id.metaData.timezone',
                'city_name': '$_id.metaData.city_name',
            }
        },
        {
            '$sort': {'_id.dt_iso': 1}
        }
    ]


def weather_entity(item) -> dict:
    timezone = 0.0 if not item['timezone'] or item['timezone'] == None else item['timezone']

    if isinstance(timezone, tuple):
        timezone = int(''.join(map(str, timezone)))

    return {
        'time': datetime.fromisoformat(str(item['dt_iso'])) + timedelta(seconds=int(item['timezone'])),
        'temp': float(item['temp'])if item['temp'] != None else None,
        'temp_max': float(str(item['temp_max'])) if item['temp_max'] != None else None,
        'temp_min': float(item['temp_min']) if item['temp_min'] != None else None,
        'feels_like': float(item['feels_like']) if item['feels_like'] != None else None,
        'pressure': float(item['pressure']) if item['pressure'] != None else None,
        'humidity': float(item['humidity'])if item['humidity'] != None else None,
        'wind_speed': float(item['wind_speed'])if item['wind_speed'] != None else None,
        'wind_deg': float(item['wind_deg'])if item['wind_deg'] != None else None,
        'timezone': timezone,
        'city_name': str(item['city_name']) if item['city_name'] != None else None
    }


class WeatherRepository:
    def __init__(self):
        self.mongo_connection = MongoDBConnection()
        self.weather_collection = self.mongo_connection.get_client().weather.weather

    def save(self, data):
        logger.info("Saving weather data")
        try:
            self.weather_collection.insert_one(data)
            logger.info("Weather data inserted successfully.")
        except PyMongoError as e:
            logger.error("Error inserting weather data:", e)

    def find(self, station: Station, start: str, stop: str, unit=None):
        logger.info(f"weather station: {station.name}")

        try:
            records = self.weather_collection.aggregate(
                pipeline=get_pipe(station, start, stop, unit),
                allowDiskUse=True
            )

            return [weather_entity(item) for item in records]

        except PyMongoError as e:
            logger.error("Error finding weather data:", e)
