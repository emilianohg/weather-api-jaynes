import sys
import json

from app.database.stations_repository import StationsRepository

def load_json_file(file_path):
    try:
        with open(file_path, 'r') as file:
            data = json.load(file)
            return data
    except FileNotFoundError:
        print("Error: File '{}' not found.".format(file_path))
    except json.JSONDecodeError:
        print("Error: Invalid JSON format in file '{}'.".format(file_path))

def main():
    if len(sys.argv) != 2:
        print("Usage: python load_stations.py <file_path>")

    file_path = sys.argv[1]
    data = load_json_file(file_path)

    # Imprimir los objetos del JSON
    print("Contenido del archivo JSON:")
    print(json.dumps(data, indent=2))

    # Insertar los objetos en la base de datos
    StationsRepository().save(data)

if __name__ == "__main__":
    main()