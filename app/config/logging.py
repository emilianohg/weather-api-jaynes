import os
from datetime import datetime
import logging

def getLogger(name: str, prefix: str = None):
    log_file_name = datetime.now().strftime("%Y-%m-%d") + ".log"

    if prefix is not None:
        log_file_name = f"{prefix}-{log_file_name}"

    log_file_path = os.path.join('app/logs', log_file_name)

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(message)s",
        handlers=[
            logging.FileHandler(log_file_path),
            logging.StreamHandler()
        ]
    )

    return logging.getLogger(name)